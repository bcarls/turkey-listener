#!/bin/bash

SERVICE='turkey-listener.py'

if ps ax | grep -v grep | grep $SERVICE > /dev/null
then
  echo "$SERVICE service running, all is well!"
else
  echo "$SERVICE is not running"
  echo "Twitter bot is not running, restarting it." | mail -s "Starting twitter bot" bcarls@gmail.com
  cd /home/bcarls/turkey

  # Create logile for output
  file_name=bot_logfile.log
  current_time=$(date "+%Y.%m.%d-%H.%M.%S")
  echo "Current Time : $current_time"
  new_fileName=$current_time.$file_name
  echo "Log file name: " "$new_fileName"
  python3 turkey-listener.py -s True &> $new_fileName
fi
