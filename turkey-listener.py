
import sys
from datetime import datetime, date, time
import time
import numpy
import pytz
import pickle
import tweepy
import threading
from keys import twitter_keys




def main(argv):


    tweet_type = -1 
    xml_directory = ''
    post_tweet = False
    record_data = False
    stream_to_respond = False

    # Parse arguments
    args = argv[1:]
    while len(args) > 0:
        if args[0] == '-t' or args[0] == '--tweet-type':
            if len(args) > 1:
                tweet_type = int(args[1])
                del args[0:2]
        elif args[0] == '-x' or args[0] == '--xml-directory':
            if len(args) > 1:
                xml_directory = args[1]
                del args[0:2]
        elif args[0] == '-p' or args[0] == '--post-tweet':
            if len(args) > 1:
                if str(args[1]) == 'True':
                    post_tweet = True
                del args[0:2]
        elif args[0] == '-r' or args[0] == '--record-data':
            if len(args) > 1:
                if str(args[1]) == 'True':
                    record_data = True
                del args[0:2]
        elif args[0] == '-s' or args[0] == '--stream_to_respond':
            if len(args) > 1:
                if str(args[1]) == 'True':
                    stream_to_respond = True
                del args[0:2]



    if post_tweet == False:
        print('Not posting the tweet. This is default. Specify --post-tweet True to post.')
    elif post_tweet == True:
        print('Will post the tweet.')

    if post_tweet == False:
        print('Not recording data. This is default. Specify --record-data True to post.')
    elif post_tweet == True:
        print('Will record data.')

    if tweet_type < 0:
        print('No tweet-type specified. Selecting automatically.')





    # This starts the stream which allows response to tweets
    consumer_key = twitter_keys['consumer_key']
    consumer_secret = twitter_keys['consumer_secret']
    access_token = twitter_keys['access_token']
    access_token_secret = twitter_keys['access_token_secret']

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    myStreamListener = MyStreamListener()
    if stream_to_respond:
        myStream = tweepy.Stream(auth = api.auth, listener=myStreamListener)
        myStream.filter(track=['turkey'],async=True)







    # print('PeriodicTweetsThread thread has ended, exiting.')

    # Disconnect the Stream
    # When the PeriodicTweetsThread thread stops, the streaming thread also needs
    # to be killed. This is done by sending "Sleep." to the bot to trigger the on_status 
    # command which will check if disconnect was previously given and actually do the disconnect. 
    # if stream_to_respond:
    #     myStream.disconnect()
    #     status = bot_utils.post_tweet('@L_Tron_CTA Sleep.')
    #     api.destroy_status(status.id)







#override tweepy.StreamListener to add logic to on_status

class MyStreamListener(tweepy.StreamListener):

    # def test(self, tweet, user_reply):
    #     global train_statuses
    #     print('\n')
    #     print("MyStreamListener Test")


    def on_error(self, status):
        print(str(datetime.now()) + ' -- ' + 'Stream Error')
        print(status)
        self.running = False
        return

    def on_timeout(self):
        print(str(datetime.now()) + ' -- ' + 'Stream Timeout')
        self.running = False
        return
        
    def on_disconnect(self, notice):
        print(str(datetime.now()) + ' -- ' + 'Stream Disconnect')
        print(notice)
        self.running = False
        return

    def on_status(self, status):
        tweet = status.text
        print('\n')
        print(str(datetime.now()) + ' -- ' + "MyStreamListener")
        print(tweet)
        # corpus = pandas.read_table("corpus.data", delim_whitespace = True)


        # Record the time when a tweet might be made
        dt_tweet = datetime.now(pytz.timezone('US/Central'))








if __name__ == '__main__':
    sys.exit(main(sys.argv))

